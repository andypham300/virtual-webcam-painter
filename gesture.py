import mediapipe as mp
import cv2 as cv
import HandTrackerModule as htm

BaseOptions = mp.tasks.BaseOptions
GestureRecognizer = mp.tasks.vision.GestureRecognizer
GestureRecognizerOptions = mp.tasks.vision.GestureRecognizerOptions
GestureRecognizerResult = mp.tasks.vision.GestureRecognizerResult
VisionRunningMode = mp.tasks.vision.RunningMode

def print_result(result: GestureRecognizerResult, output_image: mp.Image, timestamp_ms: int):
    print('gesture recognition result: {}'.format(result))

options = GestureRecognizerOptions(
    base_options=BaseOptions(model_asset_path='gesture_recognizer.task'),
    running_mode=VisionRunningMode.LIVE_STREAM,
    result_callback=print_result)

capture = cv.VideoCapture(0)
handDetector = htm.handDetection()

while True:
    isTrue, frame = capture.read()
    frame = cv.flip(frame, 1)
    frame = handDetector.findHands(frame)

    mp_image = mp.Image(image_format=mp.ImageFormat.SRGB, data=frame)

    with GestureRecognizer.create_from_options(options) as recognizer:
        recognizer.recognize_async(mp_image)

    cv.imshow('Webcam', frame)

    if cv.waitKey(20) & 0xFF == ord("x"):
        break

capture.release()
cv.destroyAllWindows()

