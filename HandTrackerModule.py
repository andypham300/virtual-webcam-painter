import cv2 as cv
import mediapipe as mp
import time

class handDetection():
    def __init__(self, mode = False, maxHands = 2, modelComplexity = 1, detectionConfidence = 0.5, trackingConfidence = 0.5):
        self.mode = mode
        self.maxHands = maxHands
        self.modelComplexity = modelComplexity
        self.detectionConfidence = detectionConfidence
        self.trackingConfidence = trackingConfidence

        self.fingerIDs = [4, 8, 12, 16, 20]

        self.mpDraw = mp.solutions.drawing_utils
        self.mpHands = mp.solutions.hands
        self.hands = self.mpHands.Hands(self.mode, self.maxHands, self.modelComplexity, self.detectionConfidence, self.trackingConfidence)

    def findHands(self, frame, draw = True):
        frameRGB = cv.cvtColor(frame, cv.COLOR_BGR2RGB)
        self.handTrack = self.hands.process(frameRGB)

        if self.handTrack.multi_hand_landmarks:
            for handLandmarks in self.handTrack.multi_hand_landmarks:
                if draw:
                    self.mpDraw.draw_landmarks(frame, handLandmarks, self.mpHands.HAND_CONNECTIONS)

        return frame
    
    def findPosition(self, frame, handNum = 0):
        self.landmarkList = []
        if self.handTrack.multi_hand_landmarks:
            hand = self.handTrack.multi_hand_landmarks[handNum]
            for id, landmark in enumerate(hand.landmark):
                h, w, c = frame.shape
                cx, cy = int(landmark.x * w), int(landmark.y * h)
                self.landmarkList.append([id, cx, cy])

        return self.landmarkList
    
    def ifFingerIsUpDetection(self):
        fingers = []

        if (self.landmarkList[self.fingerIDs[0]][1] < self.landmarkList[self.fingerIDs[0] - 1][1]):
            fingers.append(1)
        else:
            fingers.append(0)
        
        for i in range(1,5):
            if (self.landmarkList[self.fingerIDs[i]][2] < self.landmarkList[self.fingerIDs[i] - 3][2]):
                fingers.append(1)
            else:
                fingers.append(0)
                
        return fingers


def main():
    capture = cv.VideoCapture(0)

    prevTime = 0
    currentTime = 0

    hands = handDetection()

    while True:
        isTrue, frame = capture.read()
        frame = cv.flip(frame, 1)

        frame = hands.findHands(frame)

        landmarkList = hands.findPosition(frame)
        fingersUpList = hands.ifFingerIsUpDetection()
        if landmarkList:
            print(fingersUpList)

        currentTime = time.time()
        fps = 1 / (currentTime - prevTime)
        prevTime = currentTime

        cv.putText(frame, str(int(fps)), (10, 50), cv.FONT_HERSHEY_PLAIN, 3, (0, 255, 0), 3)

        cv.imshow('Webcam', frame)

        if cv.waitKey(20) & 0xFF == ord("x"):
            break

    capture.release()
    cv.destroyAllWindows()

if __name__ == "__main__":
    main()