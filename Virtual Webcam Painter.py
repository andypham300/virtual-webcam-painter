import cv2 as cv
import numpy as np
import HandTrackerModule as htm

def changeRes(captureSource, width, height):
    captureSource.set(3, width)
    captureSource.set(4, height)

paintSelectionHeaderPath = 'images/paint_selection.jpg'
paintSelectionHeader = cv.imread(paintSelectionHeaderPath)

capture = cv.VideoCapture(0)
changeRes(capture, 1600, 896)

handDetector = htm.handDetection(detectionConfidence=0.25)

BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
RED = (0, 0, 255)
ORANGE = (0, 127, 255)
YELLOW = (0, 255, 255)
GREEN = (0, 255, 0)
BLUE = (255, 0, 0)
INDIGO = (130, 0, 75)
VIOLET = (211, 0, 148)

UNFILLED = 2
FILLED = cv.FILLED

brushCursor = FILLED
prevColor = WHITE

currentColor = WHITE
eraserThickness = 75
brushThickness = 10
currentBrushThickness = brushThickness

xIndexPrev, yIndexPrev = 0, 0
xMiddlePrev, yMiddlePrev = 0, 0

canvas = np.zeros((896, 1600, 3), np.uint8)

while True:
    isTrue, frame = capture.read()
    frame = cv.flip(frame, 1)
    frame[0:140, 0:1600] = paintSelectionHeader

    frame = handDetector.findHands(frame, draw=False)

    landmarkList = handDetector.findPosition(frame)
    if landmarkList:
        xIndex, yIndex = landmarkList[8][1:]
        xMiddle, yMiddle = landmarkList[12][1:]

        fingers = handDetector.ifFingerIsUpDetection()

        if fingers == [0, 1, 1, 0, 0]:
            if yIndex < 140:
                if 180 < xIndex < 295:
                    prevColor = currentColor
                    currentColor = WHITE

                elif 330 < xIndex < 445:
                    prevColor = currentColor
                    currentColor = RED
                
                elif 440 < xIndex < 600:
                    prevColor = currentColor
                    currentColor = ORANGE
                
                elif 635 < xIndex < 755:
                    prevColor = currentColor
                    currentColor = YELLOW
                
                elif 790 < xIndex < 905:
                    prevColor = currentColor
                    currentColor = GREEN

                elif 940 < xIndex < 1060:
                    prevColor = currentColor
                    currentColor = BLUE

                elif 1095 < xIndex < 1215:
                    prevColor = currentColor
                    currentColor = INDIGO

                elif 1250 < xIndex < 1365:
                    prevColor = currentColor
                    currentColor = VIOLET
                
                elif 1450 < xIndex < 1575:
                    currentColor = BLACK
                    brushCursor = UNFILLED
                    prevColor = currentColor
            
            cv.rectangle(frame, (xIndex, yIndex - 25), (xMiddle, yMiddle + 25), currentColor, brushCursor)
                

        if fingers == [0, 1, 0, 0, 0]:
            currentColor = prevColor
            if currentColor == BLACK:
                brushCursor = UNFILLED
                currentBrushThickness = eraserThickness
            else:
                brushCursor = FILLED
                currentBrushThickness = brushThickness

            cv.circle(frame, (xIndex, yIndex), 20, currentColor, brushCursor)
            
            if xIndexPrev == 0 & yIndexPrev == 0:
                xIndexPrev, yIndexPrev = xIndex, yIndex

            cv.line(frame, (xIndexPrev, yIndexPrev), (xIndex, yIndex), currentColor, currentBrushThickness)
            cv.line(canvas, (xIndexPrev, yIndexPrev), (xIndex, yIndex), currentColor, currentBrushThickness)
        
            xIndexPrev, yIndexPrev = xIndex, yIndex
        else:
            xIndexPrev, yIndexPrev = 0, 0
        
        if fingers == [0, 1, 1, 1, 0]:
            currentColor = BLACK
            brushCursor = UNFILLED
            currentBrushThickness = eraserThickness

            cv.circle(frame, (xMiddle, yMiddle), 20, currentColor, brushCursor)

            if xIndexPrev == 0 & yIndexPrev == 0:
                xMiddlePrev, yMiddlePrev = xMiddle, yMiddle

            cv.line(frame, (xMiddlePrev, yMiddlePrev), (xMiddlePrev, yMiddlePrev), currentColor, currentBrushThickness)
            cv.line(canvas, (xMiddlePrev, yMiddlePrev), (xMiddlePrev, yMiddlePrev), currentColor, currentBrushThickness)

            xMiddlePrev, yMiddlePrev = xMiddle, yMiddle

    grayFrame = cv.cvtColor(canvas, cv.COLOR_BGR2GRAY)
    _, frameInverse = cv.threshold(grayFrame, 10, 255, cv.THRESH_BINARY_INV)
    frameInverse = cv.cvtColor(frameInverse, cv.COLOR_GRAY2BGR)
    frame = cv.bitwise_and(frame, frameInverse)
    frame = cv.bitwise_or(frame, canvas)

    cv.imshow('Webcam Painter', frame)

    if cv.waitKey(20) & 0xFF == ord("x"):
        break

capture.release()
cv.destroyAllWindows()