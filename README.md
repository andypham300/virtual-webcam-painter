# Virtual Webcam Painter
Uses user webcam as a canvas and finger to paint through MediaPipe's hand recognition. Developed in Python on OpenCV and MediaPipe.

## How to Use
- Put up your index finger to paint
- Put up both your index and middle finger to select a color or the eraser tool at the top.
- Put up your index, middle, and ring finger to switch to the eraser tool
- Exit program by pressing 'x' 

## Future Plans
- Gesture recognition for custom actions
- Paint bucket tool
- Clear canvas button

## How it Looks

[![Virtual Webcam Painter Demo](https://img.youtube.com/vi/OqOSG__sOo4/sddefault.jpg)](https://youtu.be/OqOSG__sOo4 "Virtual Webcam Painter Demo")
